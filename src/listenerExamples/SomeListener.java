package listenerExamples;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
/**
 * A very simple listener that just prints a message when it is informed of an event.
 * 
 * @author cusack
 *
 */
public class SomeListener implements ActionListener {
	 /**
	  * The actionPerfomed method required by the ActionListener interface.
	  */
	 public void actionPerformed(ActionEvent e) {
		 JOptionPane.showMessageDialog(null,
				 "Somebody pressed the button",
				 "Button Pressed",
				 JOptionPane.INFORMATION_MESSAGE);
	 }
}
